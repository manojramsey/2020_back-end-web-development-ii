const request = require("request");

const url =
  "http://api.weatherstack.com/current?access_key=8d654b6409319fce47aaccb391dcb1c9&query=38.871417,-99.344556&units=f";

request({ url: url, json: true }, (error, response) => {
  //console.log(response);
  // const data = JSON.parse(response.body);
  // console.log(data.current);
  //console.log(response.body.current);

  if (error) {
    console.log("Unable to connect to the weather service");
  } else if (response.body.error) {
    console.log("unable to find location");
  } else {
    console.log(
      response.body.current.weather_descriptions[0] +
        ". It is currently " +
        response.body.current.temperature +
        " degrees out there and it feels like " +
        response.body.current.feelslike +
        " There is a " +
        response.body.current.precip +
        "% chance of rain "
    );
  }
});

const geocodeURL =
  "https://api.mapbox.com/geocoding/v5/mapbox.places/hays.json?access_token=pk.eyJ1IjoiYWtoaWxrdW1hcjkxMTQzOTEiLCJhIjoiY2tmbW44OHZ0MDBnZjJxczE4bmVlcHlhdyJ9.KShmRDrk3Yh8jKCQKb3Ntw&limit=1";

request({ url: geocodeURL, json: true }, (error, response) => {
  if (error) {
    console.log("Unable to connect to location services!");
  } else if (response.body.features.length === 0) {
    console.log("Unable to find location. Try another search");
  } else {
    const latitude = response.body.features[0].center[0];
    const longitude = response.body.features[0].center[1];
    console.log(latitude, longitude);
  }
});
