const request = require("request");

const forecast = (latitude, longitude, callback) => {
  const url =
    "http://api.weatherstack.com/current?access_key=8d654b6409319fce47aaccb391dcb1c9&query=" +
    latitude +
    "," +
    longitude;

  request({ url: url, json: true }, (error, response) => {
    if (error) {
      callback("Unable to weather service!", undefined);
    } else if (response.body.error) {
      callback("Unable to find location", undefined);
    } else {
      callback(
        undefined,
        response.body.current.weather_descriptions[0] +
          ". it is currently " +
          response.body.current.temperature +
          " degrees out there. There is a " +
          response.body.current.precip +
          "% chance of rain "
      );
    }
  });
};

module.exports = forecast;
