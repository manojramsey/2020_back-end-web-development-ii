import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users: User[];
  showExtended: boolean = true;
  loaded: boolean = false;
  enableAdd: boolean = true;
  currentClasses = {};

  constructor() {}

  ngOnInit(): void {
    this.users = [
      {
        firstName: 'Harry',
        lastName: 'Potter',
        age: 30,
        address: {
          street: '1st street',
          city: 'Hays',
          state: 'KS',
        },
        image: 'https://picsum.photos/200?random=1',
        isActive: false,
        netWorth: 200,
        birthDay: new Date('01/01/2020 09:30:00'),
      },
      {
        firstName: 'Ron',
        lastName: 'Wesley',
        age: 30,
        address: {
          street: '2nd street',
          city: 'Hays',
          state: 'KS',
        },
        image: 'https://picsum.photos/200?random=2',
        isActive: true,
        netWorth: 100,
        birthDay: new Date('01/02/2020 08:30:00'),
      },
      {
        firstName: 'Sirius',
        lastName: 'Black',
        age: 30,
        address: {
          street: '3rd street',
          city: 'Hays',
          state: 'KS',
        },
        image: 'https://picsum.photos/200?random=3',
        isActive: false,
        netWorth: 100,
        birthDay: new Date('01/03/2020 10:30:00'),
      },
    ];
    this.loaded = true;
    this.showExtended = true;

    this.addUser({
      firstName: 'Albus',
      lastName: 'Dumbledore',
      age: 30,
    });

    this.setCurrentClasses();
  }
  addUser(user: User) {
    this.users.push(user);
  }

  setCurrentClasses() {
    this.currentClasses = {
      'btn-success': this.enableAdd,
    };
  }
}
