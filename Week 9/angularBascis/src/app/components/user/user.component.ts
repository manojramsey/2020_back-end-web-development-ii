import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  //properties
  user: User;

  //Methods
  constructor() {}
  ngOnInit() {
    this.user = {
      firstName: 'Harry',
      lastName: 'Potter',
      age: 30,
      address: {
        street: '1st street',
        city: 'Hays',
        state: 'KS',
      },
      image: 'https://picsum.photos/200?random=2',
    };
  }
}
