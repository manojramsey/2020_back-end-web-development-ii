const path = require("path");
const express = require("express");
const hbs = require("hbs");

const app = express();

// Define paths for Express config
const publicDirectoryPath = path.join(__dirname, "../public");
const viewsPath = path.join(__dirname, "../templates/views");
const partialsPath = path.join(__dirname, "../templates/partials");

// setup handlebars engine and views location
app.set("view engine", "hbs");
app.set("views", viewsPath);
hbs.registerPartials(partialsPath);

//Setup static directory to serve
app.use(express.static(publicDirectoryPath));

//app.com
app.get("", (req, res) => {
  res.render("index", {
    title: "Weather APP",
    name: "Sam",
  });
});

//app.com/about
app.get("/about", (req, res) => {
  res.render("about", { title: "About Page", name: "Sam" });
});

//app.com/help
app.get("/help", (req, res) => {
  res.render("help", {
    title: "Help Page",
    name: "Sam",
    email: "sam@gmail.com",
  });
});

//app.com/weather
app.get("/weather", (req, res) => {
  res.send({
    location: "Hays",
    temperature: 65,
  });
});

app.get("/help/*", (req, res) => {
  res.send("Help article is not found!");
});

app.get("*", (req, res) => {
  res.render("404", {
    title: "404",
    name: "Sam",
    errorMessage: "Page not found!",
  });
});

app.listen(3000, () => {
  console.log("Server is up and running on port 3000");
});

const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})