const yargs = require("yargs");
const validator = require("validator");
const chalk = require("chalk");

const getNotes = require("./notes");

const msg = getNotes();
const greenMsg = chalk.green("Success!");
const redMsg = chalk.blue.bgYellow.inverse.bold("Error!!");
const command = process.argv[2];

console.log(msg);
console.log(validator.isEmail("ramsey@gmail.com"));
console.log(validator.isURL("https://fhsu.edu"));
console.log(greenMsg);
console.log(redMsg);

console.log(process.argv);
if (command === "add") {
  console.log("Adding note!");
} else if (command === "remove") {
  console.log("Removing note!!");
}

yargs.version("1.1.0");

// Create add command
yargs.command({
  command: "add",
  describe: "Add a new note",
  builder: {
    title: {
      describe: "Note title",
      demandOption: true,
      type: "string",
    },
    body: {
      describe: "Note body",
      demandOption: true,
      type: "string",
    },
  },
  handler: function (argv) {
    console.log("Title: " + argv.title);
    console.log("Body: " + argv.body);
  },
});

// Create remove command
yargs.command({
  command: "remove",
  describe: "Remove a new note",

  handler: function (argv) {
    console.log("Removing the note");
  },
});
// Create read command
yargs.command({
  command: "read",
  describe: "Read a new note",

  handler: function (argv) {
    console.log("Reading a note");
  },
});
// Create list command
yargs.command({
  command: "list",
  describe: "List your notes",

  handler: function (argv) {
    console.log("Listing out all notes");
  },
});
//console.log(yargs.argv);

yargs.parse();
